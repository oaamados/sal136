Au2022::Application.config.relative_url_root = ENV.fetch(
  'RUTA_RELATIVA', '/heb412')
if Au2022::Application.config.relative_url_root == '/'
  Au2022::Application.config.assets.prefix = '/assets'
else
  Au2022::Application.config.assets.prefix = Au2022::Application.config.relative_url_root +  '/assets'
end
