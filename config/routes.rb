Rails.application.routes.draw do
  resources :tabulae14s
  rutarel = ENV.fetch('RUTA_RELATIVA', 'heb412/')
  scope rutarel do 

    devise_scope :usuario do
      get 'sign_out' => 'devise/sessions#destroy'

      # El siguiente para superar mala generación del action en el formulario
      # cuando se monta en sitio diferente a / y se autentica mal (genera 
      # /puntomontaje/puntomontaje/usuarios/sign_in )
      if (Rails.configuration.relative_url_root != '/') 
        ruta = File.join(Rails.configuration.relative_url_root, 
                         'usuarios/sign_in')
        post ruta, to: 'devise/sessions#create'
        get  ruta, to: 'devise/sessions#new'
      end
      get 'sign_up' => 'registrations#new', as: :sign_up
    end
    devise_for :usuarios, module: :devise,
      controllers: { registrations: "registrations" }
    as :usuario do
      get 'usuarios/edit' => 'devise/registrations#edit', 
        :as => 'editar_registro_usuario'    
      put 'usuarios/:id' => 'devise/registrations#update', 
        :as => 'registro_usuario'            
    end
    resources :usuarios, path_names: { new: 'nuevo', edit: 'edita' } 

    resources :elecciones, path_names: { new: 'nueva', edit: 'edita' } 

    get 'gracias' => 'sip/hogar#gracias', as: :gracias

    get 'ppd' => 'sip/hogar#ppd', as: :ppd 

    get '/tabulae14s/nuevomismopuesto/:tabulae14_id' =>
      'tabulae14s#nuevomismopuesto',
       as: :tabulae14_nuevomismopuesto

    get '/elecciones/:id/nacional' => 'elecciones#nacional',
      as: :elecciones_nacional
    get '/elecciones/:id/departamental/:coddep' => 'elecciones#departamental',
      as: :elecciones_departamental
    get '/elecciones/:id/municipal/:coddep/:codmun' => 'elecciones#municipal',
      as: :elecciones_municipal
    get '/elecciones/:id/zona/:coddep/:codmun/:zona' => 'elecciones#zona',
      as: :elecciones_zona
    get '/elecciones/:id/puesto/:coddep/:codmun/:zona/:puesto' => 
    'elecciones#puesto', as: :elecciones_puesto
    get '/elecciones/:id/mesa/:coddep/:codmun/:zona/:puesto/:mesa' => 
    'elecciones#mesa', as: :elecciones_mesa

    root 'sip/hogar#index'

    namespace :admin do
      ab = ::Ability.new
      ab.tablasbasicas.each do |t|
        if (t[0] == '') 
          c = t[1].pluralize
          resources c.to_sym, 
            path_names: { new: 'nueva', edit: 'edita' }
        end
      end
    end
  end

  mount Heb412Gen::Engine, at: rutarel, as: 'heb412_gen'
  mount Mr519Gen::Engine, at: rutarel, as: 'mr519_gen'
  mount Sip::Engine, at: rutarel, as: 'sip'
end
