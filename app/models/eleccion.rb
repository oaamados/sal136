class Eleccion < ApplicationRecord
  campofecha_localizado :fechaini
  campofecha_localizado :fechafin

  validates :nombre, presence: true, length: {maximum: 255}
  validates :numcandidatos, presence: true, numericality: {greater_than: 1,
  less_than: 13}
  validates :fechaini, presence: true
  validates :fechafin, presence: true, 
    comparison: {greater_than_or_equal_to: :fechaini}

  validate :valida_numcandidatos_candidatos

  def valida_numcandidatos_candidatos
    (1..numcandidatos).each do |n|
      if self['candidato' + n.to_s].nil?
        errors.add("Como son #{numcandidatos} el #{n} no debería estar vació")
      end
    end
    (numcandidatos+1..12).each do |n|
      if !self['candidato' + n.to_s].nil?
        errors.add("Como son #{numcandidatos} el #{n} debería estar vació")
      end
    end
  end
 
end

