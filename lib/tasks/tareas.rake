# encoding: UTF-8

require 'active_support/core_ext/object/inclusion'
require 'active_record'
require 'colorize'

#require 'byebug'

#require_relative '../../app/helpers/sip/tareasrake_helper'

desc "Vuelca datosindices"
task vuelcadatos: :environment do
  connection = ActiveRecord::Base.connection();
  puts "datos"

  maq = '-h ' + ENV.fetch('BD_SERVIDOR') + ' -U ' + ENV.fetch('BD_USUARIO')
  Sip::TareasrakeHelper::asegura_varambiente_bd
  connection = ActiveRecord::Base.connection()
  nomarc = "db/datos.sql"
  File.open(nomarc, "w") { |f| 
    f << "-- Volcado de datos de elecciones populares en Colombia tabulados por voluntarios en https://sal136.pasosdeJesus.org\n"
    f << "-- Dominio público de acuerdo a la legislación colombiana.  https://www.pasosdejesus.org/dominio_publico_colombia.html\n\n"
  }
  puts "usuario"
  command = "pg_dump --inserts --data-only --no-privileges " +
    "--no-owner --column-inserts --table=usuario " +
    "#{maq} #{Shellwords.escape(ENV.fetch('BD_NOMBRE'))} " +
    "| grep -v \"INTO public.usuario (nusuario, password, nombres, descripcion, rol, idioma, id, .*([^,]*, [^,]*, [^,]*, [^,]*, [^,]*, [^,]*, 1\" " +
    "| sed -e \"s/\(.*INTO public.usuario (nusuario, password, nombres, descripcion, rol, idioma, id, fechacreacion, fechadeshabilitacion, email, encrypted_password,.*([^,]*, [^,]*, [^,]*, [^,]*, [^,]*, [^,]*, [^,]*, [^,]*, [^,]*, [^,]*, \)[^,]*\(,.*)\)/\1''\2/g\" " +
    "| sed -e \"s/SET lock_timeout = 0;//g\" >> #{nomarc}"
  puts command.green
  raise "Error al volcar tabla usuario" unless Kernel.system(command)
  puts "eleccion"
  command = "pg_dump --inserts --data-only --no-privileges " +
    "--no-owner --column-inserts --table=eleccion " +
    "#{maq} #{Shellwords.escape(ENV.fetch('BD_NOMBRE'))} " +
    "| sed -e \"s/SET lock_timeout = 0;//g\" >> #{nomarc}"
  puts command.green
  raise "Error al volcar tabla eleccion" unless Kernel.system(command)
  puts "tabulae14"
  command = "pg_dump --inserts --data-only --no-privileges " +
    "--no-owner --column-inserts --table=tabulae14 " +
    "#{maq} #{Shellwords.escape(ENV.fetch('BD_NOMBRE'))} " +
    "| sed -e \"s/SET lock_timeout = 0;//g\" >> #{nomarc}"
  puts command.green
  raise "Error al volcar tabla tabulae14" unless Kernel.system(command)


end



