module EleccionesHelper

  def promedio(campo, filtro)
    if filtro.include?(:municipio_id)
      s = Prome14.select("sum(#{campo})").where(filtro).to_sql 
    elsif filtro.include?(:departamento_id)
      s = Prome14.select("sum(#{campo})").
        joins('JOIN sip_municipio ON sip_municipio.id=prome14.municipio_id').
          where('sip_municipio.id_departamento=?', filtro[:departamento_id]).
          to_sql
    else  #Total nacional
      s = Prome14.select("sum(#{campo})").to_sql
    end

    p = Prome14.connection.execute(s)[0]['sum'].to_f
    pr = (p*100).to_i/100.0
    return pr.a_decimal_localizado
  end



  # De https://stats.stackexchange.com/questions/117741/adding-two-or-more-means-and-calculating-the-new-standard-deviation
  # se generaliza que cuando X_i son independientes E(sum X_i) = sum E(X_i)
  # y Var(sum X_i) = sum(Var(X_i))
  def desviacion(campo, filtro)
    if filtro.include?(:municipio_id)
      s = Desve14.select("sqrt(sum(#{campo}*#{campo}))").where(filtro).to_sql 
    elsif filtro.include?(:departamento_id)
      s = Desve14.select("sqrt(sum(#{campo}*#{campo}))").
        joins('JOIN sip_municipio ON sip_municipio.id=desve14.municipio_id').
          where('sip_municipio.id_departamento=?', filtro[:departamento_id]).
          to_sql
    else  #Total nacional
      s = Desve14.select("sqrt(sum(#{campo}*#{campo}))").to_sql
    end

    p = Desve14.connection.execute(s)[0]['sqrt'].to_f
    pr = (p*100).to_i/100.0
    return pr.a_decimal_localizado
  end


end
