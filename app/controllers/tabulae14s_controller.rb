class Tabulae14sController < ApplicationController
  before_action :set_tabulae14, only: %i[ show edit update destroy ]

  def clase
    "::Tabulae14"
  end

  # Genero del modelo (F - Femenino, M - Masculino)
  def genclase
    return 'M';
  end

  def atributos_index
    atributos_show
  end

  def new
    Sip::Municipio.conf_presenta_nombre_con_departamento = true

    if cannot? :new, clase.constantize
      # Supone alias por omision de https://github.com/CanCanCommunity/cancancan/blob/develop/lib/cancan/ability/actions.rb
      authorize! :create, clase.constantize
    end
    @registro = clase.constantize.new(tabulae14_params)
    if @registro.respond_to?('current_usuario=')
      @registro.current_usuario = current_usuario
    end
    if @registro.respond_to?(:fechacreacion)
      @registro.fechacreacion = DateTime.now.strftime('%Y-%m-%d')
    end

    nuevo_intermedio(@registro)

    if registrar_en_bitacora
      Sip::Bitacora.a(request.remote_ip, current_usuario.id,
                      request.url, params, @registro.class.to_s,
                      nil,  'iniciar', '')
    end

    render layout: 'application'
  end

  def atributos_show
    r = [ 
      :id,
      :eleccion_id, 
      :usuario_id, 
      :municipio_id, 
      :zona, 
      :puesto, 
      :mesa, 
      :totalsufragantes, 
      :totalurna, 
      :totalincinerados, 
      :c1, :c2, :c3, :c4, :c5, :c6, 
      :c7, :c8, :c9, :c10, :c11, :c12, 
      :blanco, 
      :nulos, 
      :nomarcados, 
      :votosmesa, 
      :otrasconstancias, 
      :recuento_id, 
      :solicitadopor, 
      :enrepde, 
      :ccjurado1, 
      :ccjurado2, 
      :ccjurado3, 
      :ccjurado4, 
      :ccjurado5, 
      :ccjurado6
    ]
    return r
  end

  def validaciones(registro)
      @validaciones_error = ''
    if registro.usuario_id.nil?
      registro.usuario_id = current_usuario.id
    elsif registro.usuario_id != current_usuario.id && 
      current_usuario.rol != ROLADMIN
      # No ocurre
      debugger
    end
    if registro.eleccion_id.nil?
      registro.eleccion_id = Rails.configuration.x.eleccion
    elsif registro.eleccion_id != Rails.configuration.x.eleccion &&
      current_usuario.rol != ROLADMIN
      # No ocurre
      debugger
    end

    return true
  end

  def atributos_form
    Sip::Municipio.conf_presenta_nombre_con_departamento = true
    r = atributos_show - [:id]
    if !can?(:manage, :Tabulae14)
      r -= [:usuario_id]
    end
    return r
  end

  def filtra_ca(reg)
    reg = reg.habilitados
    return reg
  end

  def vistas_manejadas
    ['Tabulae14']
  end

  def nuevomismopuesto
    if params && params[:tabulae14_id] && 
        ::Tabulae14.where(id: params[:tabulae14_id].to_i).count == 1
      e = ::Tabulae14.find(params[:tabulae14_id].to_i)
      redirect_to new_tabulae14_path(tabulae14: {
        municipio_id: e.municipio_id,
        zona: e.zona,
        puesto: e.puesto,
        mesa: (::Tabulae14.where(
          usuario_id: current_usuario.id,
          municipio_id: e.municipio_id,
          zona: e.zona,
          puesto: e.puesto
        ).maximum(:mesa)) + 1
      })
    end
  end

  private
    def set_tabulae14
      @tabulae14 = @registro = Tabulae14.find(params[:id])
    end

    def lista_params
      atributos_form
    end

    # Only allow a list of trusted parameters through.
    def tabulae14_params
      params.fetch(:tabulae14, {}).permit(atributos_form)
    end
end
