require "test_helper"

class EleccionesControllerTest < ActionDispatch::IntegrationTest

  include Devise::Test::IntegrationHelpers

  setup  do
    if ENV['CONFIG_HOSTS'] != 'www.example.com'
      raise 'CONFIG_HOSTS debe ser www.example.com'
    end
    @current_usuario = ::Usuario.find(1)
    sign_in @current_usuario
    @eleccion = ::Eleccion.create!(PRUEBA_ELECCION)
  end

  # Cada prueba que se ejecuta se hace en una transacción
  # que después de la prueba se revierte

  test "debe presentar listado" do
    get elecciones_path
    assert_response :success
    assert_template :index
  end

  test "debe presentar resumen de existente" do
    get eleccion_url(@eleccion.id)
    assert_response :success
    assert_template :show
  end

  test "debe presentar formulario para nueva" do
    get new_eleccion_path
    assert_response :success
    assert_template :new
  end

  test "debe presentar formulario de edición" do
    get edit_eleccion_path(@eleccion)
    assert_response :success
    assert_template :edit
  end

  test "debe crear nueva" do
    # Arreglamos indice
    ::Eleccion.connection.execute <<-SQL
        SELECT setval('public.eleccion_id_seq', MAX(id)) 
          FROM public.eleccion;
    SQL
    assert_difference('Eleccion.count') do
      post elecciones_path, params: { 
        eleccion: { 
          id: nil,
          nombre: 'ZZ',
          numcandidatos: 3,
          candidato1: 't1',
          candidato2: 't2',
          candidato3: 't3',
          fechaini_localizada: '2/Feb/2022',
          fechafin_localizada: '3/Feb/2022'
        }
      }
    end

    assert_redirected_to main_app.eleccion_path(
      assigns(:eleccion))
  end

  test "debe actualizar existente" do
    patch main_app.eleccion_path(@eleccion.id),
      params: { 
      eleccion: { 
        id: @eleccion.id,
        nombre: 'YY'
      }
    }

      assert_redirected_to main_app.eleccion_path(assigns(:eleccion))
  end

  test "debe eliminar" do
    assert_difference('Eleccion.count', -1) do
      delete main_app.eleccion_path(Eleccion.find(1))
    end

    assert_redirected_to main_app.elecciones_path
  end

end
