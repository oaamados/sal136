class Tabulae14 < ApplicationRecord
  campofecha_localizado :fechaini
  campofecha_localizado :fechafin

  belongs_to :usuario, optional: false, validate: true
  belongs_to :eleccion, optional: false, validate: true
  belongs_to :municipio, class_name: 'Sip::Municipio',
    foreign_key: 'municipio_id', validate: true, optional: false
  belongs_to :recuento, class_name: 'Sip::Trivalente',
    foreign_key: 'recuento_id', validate: true, optional: false

  validates :zona, presence: true
  validates :puesto, presence: true
  validates :mesa, presence: true
  validates :totalsufragantes, presence: true
  validates :totalurna, presence: true
  validates :totalincinerados, presence: true

  validates :c1, allow_nil: true, numericality: {greater_than_or_equal_to: 0}
  validates :c2, allow_nil: true, numericality: {greater_than_or_equal_to: 0}
  validates :c3, allow_nil: true, numericality: {greater_than_or_equal_to: 0}
  validates :c4, allow_nil: true, numericality: {greater_than_or_equal_to: 0}
  validates :c5, allow_nil: true, numericality: {greater_than_or_equal_to: 0}
  validates :c6, allow_nil: true, numericality: {greater_than_or_equal_to: 0}
  validates :c7, allow_nil: true, numericality: {greater_than_or_equal_to: 0}
  validates :c8, allow_nil: true, numericality: {greater_than_or_equal_to: 0}
  validates :c9, allow_nil: true, numericality: {greater_than_or_equal_to: 0}
  validates :c10, allow_nil: true, numericality: {greater_than_or_equal_to: 0}
  validates :c11, allow_nil: true, numericality: {greater_than_or_equal_to: 0}
  validates :c12, allow_nil: true, numericality: {greater_than_or_equal_to: 0}
  validates :blanco, allow_nil: true, numericality: {greater_than_or_equal_to: 0}
  validates :nulos, allow_nil: true, numericality: {greater_than_or_equal_to: 0}
  validates :nomarcados, allow_nil: true, numericality: {greater_than_or_equal_to: 0}
  validates :votosmesa, allow_nil: true, numericality: {greater_than_or_equal_to: 0}
  validates :otrasconstancias, length: {maximum: 1024}
  validates :solicitadopor, length: {maximum: 255}
  validates :enrepde, length: {maximum: 255}
  
  validates :ccjurado1, allow_nil: true, numericality: {greater_than: 0}
  validates :ccjurado2, allow_nil: true, numericality: {greater_than: 0}
  validates :ccjurado3, allow_nil: true, numericality: {greater_than: 0}
  validates :ccjurado4, allow_nil: true, numericality: {greater_than: 0}
  validates :ccjurado5, allow_nil: true, numericality: {greater_than: 0}
  validates :ccjurado6, allow_nil: true, numericality: {greater_than: 0}

  validates :observaciones, length: {maximum: 1024}

  validate :no_repite_mesa
  def no_repite_mesa
    rp = ::Tabulae14.where(
      usuario_id: usuario_id, 
      eleccion_id: eleccion_id,
      municipio_id: municipio_id,
      zona: zona,
      puesto: puesto,
      mesa: mesa
    )
    if rp.count > 1 || (rp.count == 1 && !id.nil? && rp.take.id != id)
      errors.add(:mesa, "Mesa ya tabulada en el registro #{rp.take.id}")
    end
  end
end

