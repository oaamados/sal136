class Pv2022 < ActiveRecord::Migration[7.0]
  def up
    execute <<-SQL
      INSERT INTO public.eleccion (id, nombre, numcandidatos, candidato1,
      candidato2, candidato3, candidato4, candidato5, candidato6,
      candidato7, candidato8, fechaini, fechafin, created_at, updated_at) 
      VALUES (1, 'Primera Vuelta Presidente 2022', 8, 
      'RODOLFO HERNANDEZ', 'JOHN MILTON RODRÍGUEZ', 'FEDERICO GUTIERREZ',
      'SERGIO FAJARDO', 'ENRIQUE GÓMEZ MARTINEZ', 'GUSTAVO PETRO',
      'LUIS PÉREZ', 'INGRID BETANCOURT', '2022-05-23', '2022-05-29',
      '2022-06-04', '2022-06-04'
      );

      SELECT setval('public.eleccion_id_seq', 100);
    SQL
  end
  def down
    execute <<-SQL
      DELETE FROM eleccion WHERE id=1;
    SQL
  end
end
