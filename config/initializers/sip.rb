require 'heb412_gen/version'

Sip.setup do |config|
  config.ruta_anexos = ENV.fetch('SIP_RUTA_ANEXOS', 
                                 "#{Rails.root}/archivos/anexos")
  config.ruta_volcados = ENV.fetch('SIP_RUTA_VOLCADOS',
                                   "#{Rails.root}/archivos/bd")
  # En heroku los anexos son super-temporales
  if !ENV["HEROKU_POSTGRESQL_GREEN_URL"].nil?
    config.ruta_anexos = "#{Rails.root}/tmp/"
  end
  config.titulo = "Sal136 - #{Heb412Gen::VERSION}"
  config.descripcion = "Prototipo de aplicación web para verificar elecciones en Colombia mediante la tabulación colaborativa de formularios E-14 y preconteos"
  config.codigofuente = "https://gitlab.com/pasosdeJesus/sal136"
  config.urlcontribuyentes = "https://gitlab.com/pasosdeJesus/sal136/-/graphs/main"
  config.urlcreditos = "https://gitlab.com/pasosdeJesus/sal136/-/blob/main/CREDITOS.md"
  config.agradecimientoDios = "<p>
Cantaré a Jehová, <br>
Porque me ha hecho bien.
<br>
Salmo 13:6
</p>".html_safe

  config.longitud_nusuario = 255
end
