class Desve14 < ApplicationRecord

  belongs_to :eleccion, optional: false, validate: true
  belongs_to :municipio, class_name: 'Sip::Municipio',
    foreign_key: 'municipio_id', validate: true, optional: false
  belongs_to :zona, optional: false
  belongs_to :puesto, optional: false
  belongs_to :mesa, optional: false

end

